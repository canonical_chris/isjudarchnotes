# Introduction

This document is concerned with the recent finds in the archeology of
Israel and Judah.

# 

# Bibliography

These links take one to the reading notes for the books enumerated, not
to the books themselves (e.g. on Amazon.com or Google Books or
similar).

= Israel _Finkelstein_, [The Forgotten Kingdom: The Archaeology and History
 of Northern Israel](rnotes/finkelstein-forgotten.md), Atlanta
 (Society of Biblical Literature), 2013.
 
