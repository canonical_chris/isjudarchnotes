# Introduction

This document contains the reading notes to

> Israel *FINKELSTEIN*, The Forgotten Kingdom: The Archeology
> and History of Northern Israel, Atlanta 2013.

# Reading Notes

## Notes on "Introduction" (pp.1-12)

Finkelstein begins his exposition with pointing out how significantly
the Northern kingdom dominated the Southern kingdom.

> In the first half of the eighth century B.C.E, Israel ruled over the
> lion's share of the territory of the two Hebrew Kingdoms, and its
> population accounted for three quarters of the people of Irsael and
> Judah combined [Broshi and Finkelstein 1992](http://www.jstor.org/stable/1357138?seq=1#page_scan_tab_contents "M. Broshi and I. Finkelstein (1992), The Population of Palestine in Iron Age II, BASOR 287:47-60"). (p.1)

However, historically, that dominance did not last, because the
Southern Kingdom wrote the Hebrew Bible; as such, it became
_geschichtsmächtig_.

> ... even what some scholars consider as the early layers of the
> history of ancient Israel, such as the books of Samuel
> (e.g. McCarter 1994; Halpern 2001; Römer and DePury 2000, 123-28;
> Hutton 2009), were written after the northern kingdom was vanquished
> [in 720 BCE](http://en.wikipedia.org/wiki/Kingdom_of_Israel_%28Samaria%29 "The Northern Kingdom (Wikipedia)")
> by the Assyrians and its elite was deported. (p.1)

Finkelstein dates the earliest layers of the Deuteronomistic History to
the "late seventh century BCE" (p.3), but cautions that at best

> ... the northern kingdom was already a remote, vague memory over
> a century old, and this in a period with no continuity of scribal
> activity. (p.3)

Finkelstein is willing to accept that some Northern traditions made it
into the Hebrew Bible and were available to the early Deuteronomistic
historians in either oral or written form (Schiedewind 2004; Fleming
2013).

+ the Jacob cycle in Genesis (de Pury 1991)
+ the exodus tradition (van der Toorn 1996, 287-315)
+ the "Book of Saviors" now part of Judges (Richter 1966)
+ "positive traditions regarding King Saul in Samuel" (p.3)
+ the Elijah-Elisha prophetic stories in Kings

{{RCK: Finkelstein later adds to that anti-Davidic materials in
Samuel; as well as the eight-century Northern prophets Amos and
Hosea.}}

If they were written, then Samaria before 750 BCE or the temple
of YHWH at Bethel between 750 and 720 BCE.

## Paraphrased

Finkelstein points out that the basic stability of geography and
technology (with a tip-of-the-hat to Braudel) means that the patterns
repeated themselves from the Late Bronze Age into the Iron Age.

As the Amarna letters of the thirteenth century indicate, a
Canaanite city state assemblage controlled by Egypt from the south
was more or less the norm. In LB II, it was Labayu of Shechem that
was attempting to unify, in collaboration with his sons, the Jezreel
Valley into the Samarian highlands and called forth the troops
of Egypt to disperse his ambitions.

In the time of transition from LB-II to Iron Age I, the Northern the
cities emptied, often after destruction layers, both indigenously and
externally caused, and the highlands climbed in number to a population
of some 45,000 (based on the 200/ha built-over formula).  The fact
that memories such as Shiloh as a cult place (no houses, at least a
redistributive center) are handed down from these times gives an
indication that useful information can come down over the centuries
orally.

The late Iron Age I destruction layers have no individual source and
they target "New Canaan", a basically Canaanite material layer such as
Meggido Stratum VI, Relov, Keisan, Jatt and Kinneret. That destruction
layer however has no temporal unity and can therefore not be
attributed to a single event, though memories linger in the Song of
Deborah and the death of Saul at Mount Gilboa (1 Sam 31). Though the
central hill country between Jezreel and the Beer-sheba Valley were
heavily settled, the Galilee was only sparsely settled.

At the edge of late Iron-I and early Iron-II, the Gibeon-Bethel plateau
just north of Jerusalm has thirty Iron-1 sites, including Mizpah, Ai
and Kirbat Raddana. These sites distinguish themselves through
their casemat fortifications, the only ones during this time west
of the Jordan River valley, and rapid abandonment.

Finkelstein proposes that the Gibeon-Bethel plateau was targeted by
the Sheshonq campaign recorded in the Karnak Temple. Unfortunately,
the dating information is so sparse that anything more precise than
the late 10th century BCE is impossible to give---both the parameters
of Sheshonq's reign and the point of the campaign within the reign
being unclear.

Whatever Sheshonq was targeting, it was not Jerusalem. The tenth
century BCE highlands of Judea were sparsely settled, and there was no
monumental architecture there. Judah did not expand into the Shephelah
and the Beer-Sheba Valley until after 850 BCE. Therefore Finkelstein
proposes that just as the Amarna policy had targeted a highland
polity, Sheshonq was targeting the Gibeon-Bethel polity with its
influence into the Jabbock valley in Transjordania. This is
underscored by the Manahaim kings in Jabbok River Region of the
Gilead, which are on Sheshonq's list as well as in 2 Sam 2:12.  {{RCK:
The Jacob cycle belongs into the Western Gilead, with its mention of
the Jabbok and the sites of Penuel, and was first written down
probably around 720 BCE.}}

The tribal area of Benjamin remained Omrid until 842 BC, then it was
taken by King Jehoash, possibly as a vassal of Aram of
Damascus. Henceforth Juda controlled Benjamni till the exil, but this
must be a later development, because Benjamin as "Son of the South"
would otherwise apply to the most-northern part of Juda as in Dtrn-H, and not
the most southern part of *Israel. 

Finkelstein emphasizes the need to split the Saul -> David -> Solomon
line, because Saul and David were parallel rulers in different areas,
and Saul the leader of the Gibeon polity destroyed by the Egyptian
forces of Sheshonq (not the Philistines). This works out perfectly
in terms of the geography: the Saulids (Saul and Ishbaal) are active
in Ephraim and Benjamin, with influence into the Gilead, the
Jezreel Valley, and up to Beth-Shaan, an old Egyptian strong-hold,
where the battle of Mount Gilboa takes out the Saulid army.

South of the Valley of Elan, the Tell Khirbet Qaiyafa, 10km east
of Philistine Gath, the biblical Gob, has kasemat walls and belongs
to the Saulid influence as well.

There is a possibility that Jerusalem briefly projected into the
Gibeon polity after the Sheshonq campaign; that would be the only
historical see on which to base the United Monarchy claim.  At any
rate, Sheshonq seems to have favored the Tirzah township in northern
Samaria, whence Jerobeam, originally from Zeredah near Ramallah, and
the house of Omri.

Finkelstein assumes the following chronology (with all the caveats
that these things have):

* Jerobeam I: 931-909 BCE
* Nadeb: 909-908 BCE
* Baasha: 908-885 BCE
* Elah: 885-884 BCE
* Zimri: 884 BCE
* Tibni 884-880 BCE (rival of Omri)
* Omri: 884-873 BCE

The House Omri is well attested; the Mesha Stele mentions Omri, and
Ahab is in the Kurkh inscription, because of the coalition of 853 BCE
against Shalmaneser III of Assyria in the Battle of
Qarqar. Joram---together with Azahiah---is on the Tel-Dan stele (842
BCE), and Jehu, vassal of Shalmaneser III on the Black Obelisk. If
there was a written kingslist for the North, then it would have been
compiled either at Samaria or in Bethel, in the early eight century
BCE.

The rise of early Iron II-A is very obvious in the archeological
record: there are new red-slipped burnished vessels, there is a new
town layout, and there is the beginning of an iron industry.

Tirzah, now Tell El-Far'ah, was the first capital of the Omrids,
located north east of Shechem, in the highlands (1 Kg 15f). Its Period
VIIa covers its time as the capital. It possess an acropolis of just
1ha, located at the highest, most easily defended spot that is closest
to the spring. Tirzah lacks monumental architecture, typical for a
formative capital, but has an abundance of late Iron I and early Iron
IIA seals in the VIIa layer, indicating a developing bureaucracy.

Many things said about the reign of Jerobeam are projected back from
the time of Jerobeam II. Dan and Bethel were not occupied during the
time of the first, and there is no evidence of a destruction layer at
Hazor during the campaign of Ben-Hadad of Damascus either.

Omri moved the capital to Samaria and built the first palace. Ahab
continued the build-out.

With 900 BCE, both Israel and Damascus exist as fully developed
territorial kingdoms. 


# Discussion

# Appendix: Bibliography

