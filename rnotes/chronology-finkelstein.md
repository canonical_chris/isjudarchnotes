# Chronology according to Finkelstein

*Note:* This text amalgamates the chronologies of Finkelstein (2013)
and Finkelstein et al (2008)).

## LB II (Amarna Period) [= Akhenaten (1353–1336 BC)]
+ Labayu of Shechem and his sons attempt to unify the Jezreel Valley with the Central highlands and are dispersed by Egyptian troops

## ca 950 bc, probably 931 bc
+ the Pharao Sheshonq destroys a polity, possibly lead by King Saul,
that includes the central highlands, the Jezreel Valley and the Gilead
and the Jabbock River

## ca 930 bc
+ the pharao sets up Jerobeam of Zeredah near Ramallah, at Tirzah township in northern Samariah; this will become the House of Omri as rulers of the central highlands
+ Dan and Bethel are not occupied

## 884-873 bce

Omri reigns in Israel and is mentioned in the Mesha Stele.

## 853 bce

The Assyrians under Shameneser III defeat the Levantine kings
confederation including Ahab of Israel at the Battle of Qarqar.

## 842 bce

The Tel Dan stela of the king Hazael of Aram-Damascus relates his
killing of Joram King of Israel, thereby ending the Omrid dynasty, and
of Ahaziah King of Judah, whose wife Athaliah was an Omrid princess.
The towns of Dan and Bethsaida were rebuilt and came under the
influence of Damascus.

Jehu, the new ruler of Israel, becomes a vassal of Assyria instead,
as recorded in Shameneser III's black obelisk.

The southern coastal strip is occupied by Damascus after the
destruction of the Philistine city of Gath, giving Damascus control
over the Levantine copper trade with Cyprus.

The destruction of Gath allows Judah to expand into the Shephelah and
construct the fortifications in the Beer-sheba Valley.

## ca 842-836 bc

After the death of her husband Jehoram, Omride princess Athaliah
reigns first for her son Ahaziah, and then instead of him in Judah,
attempting to extinguish the Davidic house, until Jehoash is brought
back onto the throne.

## 840-810 bce

The Mesha stele reports the incursions against the house Omri,
taking the Dibon-facing forts of Jahaz and Ataroth.

## ca 815 bce

Damascus puts a siege on Samaria ruled by King Jehoahaz.

## 800 bce

King Joash of Israel counter-attacks Ben-Hadad of Damascus and defeats
him at Aphek.

## ca 750 bc

The Northern Kingdom produces written Biblical texts, either at
Samaria or at the temple of YHWH at Bethel.

Potential candidates are

  1. Jacob Cycle in Genesis
  2. the Exodus Tradition
  3. the Book of Saviors now part of Judges
  4. pro-Saul and anti-Davidic materials in Samuel
  5. the anti-Omrid prophets Elijah, Elisha and the eight-century Northern prophets Amos and Hosea

## ca 744 bc

+ Tilgath-Pileser III shifts to a strategy of active involvement into the Assyrian vassals
+ Pekah of Israel and Rezin of Damascus pressure Ahaz of Judah, who becomes an Assyrian vassal instead

## ca 732 bc

+ Assyrians conquer Aram Damascus
+ Assyrians despoil Israel of Meggido and Hazor, the Galilee and the Mediterranean coast, ruining Israel economically

## ca 729 bc

 + Hezekiah becomes co-regent with Ahaz of Judah

## ca 722 bc
  + the Assyrian und Shalmaneser V and Sargon V besiege and destroy Samariah and end the kingdom of Israel, deporting the upper echelons of society
  + the Assyrians settle Babylonians in the new Assyrian province of Samerina, especially in Southern region of Benjamin to work the important olive oil presses
    + name evidence from Bethel (Aramaic papyrus) and Gezer (cuneiform texts)
  + some forty thousand refugees from the Southern region of Benjamin stream into Judah, and possibly more refugees from other parts of Israel, doubling the population in a short period of time and increasing the size of the Jerusalem of Hesekiah by a factor of twelve (including fortifications)

## ca 716 bc

  + Hezekiah becomes sole regent of Judah
  + evidence of cult centralization in Beer-sheba Valley at the fortress of Arad, at Tel Beer-sheba and at Lachish

## ca 705 bc

  + during the aftermath of the death of Sargon II, Hezekiah attempts to throw off the Assyrian yoke

## ca 701 bc
  + siege of Lachish (see relief in Ninniveh)
  + siege of Sennacherib of Jerusalem, leading to fortification work (Broad Wall) and the construction of the Siloam tunnel
  + Hezekiah returns to vassalhood for 800 talents of silver (*contra* 300 in 2 Kgs) and loses the Shephelah, basis of the kingdom's prosperity

## ca. 697 bc
  + Hezekiah elevates Manasseh to co-regent

## ca 608 bc
 + Pharao Necho II executes King Josiah of Judah and puts his son Jehoahaz on the throne of Jerusalem

## ca 605 bc
 + The Egyptians as supporters of the Assyrians are defeated at the battle of Carchemish

## ca 598 bc
 + the Babylonians under Nebuchadnezzar II take and destroy Jerusalem and end the Southern Kingdom
 
